require "./minesweeper/*"
require "./engine"
require "ncurses"

module MinesweeperGUI
  include Minesweeper
  LibNCurses.setlocale 0, ""
  NCurses.open do
    # Params
    NCurses.notimeout true
    NCurses.noecho
    NCurses.keypad true

    # Menu
    size, bombs = 0, 0
    x = 0
    NCurses.addstr "\
Easy (10x10, 10 bombs)
Medium (15x15, 30 bombs)
Hard (25x25, 120 bombs)"
    loop do
      NCurses.move x, 0

      case ch = NCurses.getch
      when 'w', NCurses::KeyCode::UP   then x -= 1
      when 's', NCurses::KeyCode::DOWN then x += 1
      when ' ', NCurses::KeyCode::ENTER
        case choice = x
        when 0 then size, bombs = 10, 10
        when 1 then size, bombs = 15, 30
        when 2 then size, bombs = 25, 120
        end
        break
      end

      cap x, 0, 2
    end

    board = SquareMinesweeperBoard.new size.to_u32, bombs.to_u32
    game = Minesweeper.new board

    # Game
    x = y = 0
    loop do
      NCurses.erase
      NCurses.addstr game.get_display
      NCurses.move x, y
      NCurses.refresh

      case ch = NCurses.getch
      when 'w', NCurses::KeyCode::UP    then x -= 1
      when 'a', NCurses::KeyCode::LEFT  then y -= 1
      when 's', NCurses::KeyCode::DOWN  then x += 1
      when 'd', NCurses::KeyCode::RIGHT then y += 1
      when ' ', NCurses::KeyCode::ENTER then game.go x, y
      when 'f'                          then game.flag x, y
      end

      cap x, 0, size - 1
      cap y, 0, size - 1

      if game.won?
        NCurses.erase
        NCurses.addstr "You win!"
      elsif game.lost?
        NCurses.erase
        NCurses.addstr "You lose!"
      end

      break unless game.playing?
    end

    NCurses.getch
  end
end
