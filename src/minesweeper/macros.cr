macro assert(condition)
    raise "Assetion failed" unless {{condition}}
end

macro assert(condition, message)
    raise "Assertion failed: #{ {{message}} }" unless {{condition}}
end

macro cap(n, min, max)
    if {{n}} > {{max}}
      {{n}} = {{max}}
    elsif {{n}} < {{min}}
      {{n}} = {{min}}
    end
end
