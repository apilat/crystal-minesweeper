require "./minesweeper/*"
require "./board"

enum BoardState
  Hidden
  Flagged
  Visible
end

enum GameState
  Playing
  Lost
  Won
end

module Minesweeper
  class Minesweeper
    @size : UInt32
    @size_x : UInt32
    @size_y : UInt32

    def initialize(@board : MinesweeperBoard)
      @size_x, @size_y = @board.display_size
      @size = @size_x * @size_y
      @status = Array(BoardState).new @size, BoardState::Hidden
      @state = GameState::Playing
    end

    def go(x, y)
      return unless @state == GameState::Playing

      if @board.is_bomb? x, y
        @status[x * @size_x + y] = BoardState::Visible
        @state = GameState::Lost
      else
        _spread_from x, y
      end
    end

    def _spread_from(x, y)
      @status[x * @size_x + y] = BoardState::Visible
      return unless @board.get(x, y) == 0
      @board.get_adjacent(x, y).each do |ax, ay|
        if @status[ax * @size_x + ay] == BoardState::Hidden
          _spread_from ax, ay
        end
      end
    end

    def flag(x, y)
      return unless @state == GameState::Playing

      s = @status[x * @size_x + y]

      if s == BoardState::Hidden
        @status[x * @size_x + y] = BoardState::Flagged
        _check_win
      elsif s == BoardState::Flagged
        @status[x * @size_x + y] = BoardState::Hidden
      end
    end

    def _check_win
      if @board.bombs.all? { |p| @status[p] == BoardState::Flagged }
        @state = GameState::Won
      end
    end

    macro get_gamestate(state)
        def {{state}}? : Bool
          @state == GameState::{{state.stringify.capitalize.id}}
        end
    end

    get_gamestate playing
    get_gamestate lost
    get_gamestate won

    def get_display
      board_display = @board.get_display
      String.build @size do |s|
        (0...@size).each do |p|
          status = @status[p]
          board = board_display[p]

          if board == CHARACTER_EMPTY
            char = CHARACTER_EMPTY
          else
            case status
            when BoardState::Hidden  then char = CHARACTER_UNKNOWN
            when BoardState::Flagged then char = CHARACTER_FLAG
            when BoardState::Visible then char = board
            end
          end

          s << char
          if (p + 1) % @size_x == 0
            s << CHARACTER_ROW_SEP
          end
        end
      end
    end
  end
end
