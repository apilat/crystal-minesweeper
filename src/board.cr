require "./minesweeper/*"
require "random"

module Minesweeper
  abstract class MinesweeperBoard
    abstract def set(x : Int32, y : Int32, v : UInt8)
    abstract def get(x : Int32, y : Int32) : UInt8

    abstract def is_bomb?(x : Int32, y : Int32) : Bool
    abstract def bombs : Set(UInt32)

    abstract def get_adjacent(x : Int32, y : Int32) : Array(Tuple(Int32, Int32))

    abstract def display_size : Tuple(UInt32, UInt32)
    abstract def get_display : String
  end

  class SquareMinesweeperBoard < MinesweeperBoard
    @size : UInt32

    def initialize(@size, bomb_num : UInt32)
      @board = Array(UInt8).new size * size, 0
      assert bomb_num <= size * size, "too many bombs to fit on board"
      @bombs = Set(UInt32).new bomb_num

      bomb_num.times do
        until !@bombs.includes?(p = Random.rand size * size); end
        assert p.is_a?(UInt32), "generated position is not integer"
        @bombs << p
      end

      (0...size * size).each do |p|
        next if @bombs.includes? p
        x, y = _coords p
        ba = get_adjacent(x, y).select { |x, y| is_bomb? x, y }.size.to_u8
        @board[p] = ba
      end
    end

    def bombs : Set(UInt32)
      @bombs
    end

    def get_adjacent(x : Int32, y : Int32) : Array(Tuple(Int32, Int32))
      a = [] of Tuple(Int32, Int32)
      (-1..1).each do |dx|
        (-1..1).each do |dy|
          a << {x + dx, y + dy} if _pos? x + dx, y + dy
        end
      end
      a
    end

    def is_bomb?(x : Int32, y : Int32) : Bool
      @bombs.includes?(_pos x, y)
    end

    def get(x : Int32, y : Int32) : UInt8
      @board[_pos x, y]
    end

    def set(x : Int32, y : Int32, v : UInt8)
      assert _pos?(x, y), "coordinates outside board"
      @board[_pos x, y] = v
    end

    def display_size : Tuple(UInt32, UInt32)
      {@size, @size}
    end

    def get_display : String
      String.build @size * @size do |s|
        (0...@size).each do |x|
          (0...@size).each do |y|
            if is_bomb? x, y
              s << CHARACTER_BOMB
            else
              v = get x, y
              s << v.to_s
            end
          end
        end
      end
    end

    def _get(x : Int32, y : Int32, alt) : (UInt8 | typeof(alt))
      if _pos? x, y
        get x, y
      else
        alt
      end
    end

    def _pos?(x : Int32, y : Int32) : Bool
      0 <= x < @size && 0 <= y < @size
    end

    def _pos(x : Int32, y : Int32) : Int32
      assert _pos?(x, y), "coordinates outside board"
      x * @size + y
    end

    def _coords?(p : Int32) : Bool
      0 <= p < @size * @size
    end

    def _coords(p : Int32) : Tuple(Int32, Int32)
      assert _coords?(p), "coordinates outside board"
      {p / @size, p % @size}
    end
  end
end
